package com.example.githubclient

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubclient.api.GithubReposApi
import com.example.githubclient.model.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RepositoriesViewModel : ViewModel() {
    private val _repositories = MutableLiveData<List<Repository>>()
    val repositories: LiveData<List<Repository>> = _repositories
    val progressBarVisibility: MutableLiveData<Int> = MutableLiveData(View.GONE)
    private val githubService: GithubReposApi = run {
        val retrofit = Retrofit.Builder()
            .baseUrl(GithubReposApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        retrofit.create(GithubReposApi::class.java)
    }

    private val _errorMessage: MutableLiveData<String?> = MutableLiveData()

    val errorMessage: LiveData<String?> get() = _errorMessage

    fun getRepositories(orgName: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                progressBarVisibility.postValue(View.VISIBLE)
                githubService.getOrganizationRepos(orgName).enqueue(
                    object : Callback<List<Repository>> {
                        override fun onResponse(
                            call: Call<List<Repository>>,
                            response: Response<List<Repository>>
                        ) {
                            if (!response.isSuccessful) {
                                onError("No repositories found for $orgName")
                            } else {
                                progressBarVisibility.value = View.GONE
                                _repositories.postValue(response.body())
                                _errorMessage.postValue(null)
                            }
                        }

                        override fun onFailure(p0: Call<List<Repository>>, p1: Throwable) {
                            onError("Something went wrong")
                        }

                    }
                )
            }
        }
    }

    private fun onError(errorMessage: String) {
        progressBarVisibility.postValue(View.GONE)
        this._errorMessage.postValue(errorMessage)
        _repositories.postValue(emptyList())
    }
}