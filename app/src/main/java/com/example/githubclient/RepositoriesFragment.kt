package com.example.githubclient

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubclient.databinding.FragmentRepositoriesBinding

class RepositoriesFragment : Fragment() {
    private var _binding: FragmentRepositoriesBinding? = null
    private val binding get() = _binding!!
    private val viewModel by viewModels<RepositoriesViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRepositoriesBinding.inflate(inflater, container, false)

        binding.repoList.adapter = RepositoriesAdapter(emptyList(), this::onRepoClickListener)
        binding.repoList.layoutManager = LinearLayoutManager(context)

        viewModel.repositories.observe(viewLifecycleOwner) {
            (binding.repoList.adapter as RepositoriesAdapter).setData(it)
        }
        viewModel.progressBarVisibility.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = it
        }
        viewModel.errorMessage.observe(viewLifecycleOwner) {
            it?.toToast(requireContext())
        }
        binding.performSearchButton.setOnClickListener {
            val orgName = binding.organizationNameInput.text.toString()
            viewModel.getRepositories(orgName)
        }
        return binding.root
    }

    private fun onRepoClickListener(orgName: String, repoName: String) {
        val moveToRepositoryDetailsAction = RepositoriesFragmentDirections
            .actionRepositoriesFragmentToRepositoryDetailsFragment(orgName, repoName)
        findNavController()
            .navigate(moveToRepositoryDetailsAction)
    }
}