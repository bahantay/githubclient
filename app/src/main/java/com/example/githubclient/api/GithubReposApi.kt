package com.example.githubclient.api

import com.example.githubclient.model.Repository
import com.example.githubclient.model.RepositoryDetails
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubReposApi {
    @GET("orgs/{org}/repos")
    fun getOrganizationRepos(@Path("org") org: String): Call<List<Repository>>

    @GET("repos/{org}/{repo}")
    fun getRepositoryDetails(@Path("org") org: String, @Path("repo") repo: String): Call<RepositoryDetails>

    companion object {
        const val BASE_URL = "https://api.github.com/"
    }
}