package com.example.githubclient.model

import com.google.gson.annotations.SerializedName

data class RepositoryDetails(
    val name: String,
    val description: String,
    @SerializedName("forks_count") val numberOfForks: Int,
    @SerializedName("watchers_count") val numberOfWatchers: Int,
    @SerializedName("open_issues_count") val openIssues: Int,
    @SerializedName("fork") val isFork: Boolean = IS_FORK_DEFAULT_VALUE,
    val parent: Repository? = PARENT_REPOSITORY_DEFAULT_VALUE
) {
    private companion object {
        const val IS_FORK_DEFAULT_VALUE = false
        val PARENT_REPOSITORY_DEFAULT_VALUE = null
    }
}
