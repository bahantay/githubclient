package com.example.githubclient.model

import com.google.gson.annotations.SerializedName

data class Repository(
    @SerializedName("full_name") val fullName: String,
    val description: String?
) {
    private val fullNameParts get() = fullName.split(FULL_NAME_PARTS_DELIMITER)
    val organization: String get() = fullNameParts.first()
    val repositoryName: String get() = fullNameParts.last()

    private companion object {
        const val FULL_NAME_PARTS_DELIMITER = "/"
    }
}