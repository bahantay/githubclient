package com.example.githubclient

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubclient.api.GithubReposApi
import com.example.githubclient.model.RepositoryDetails
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RepositoryDetailsViewModel : ViewModel() {
    private val _repositoryDetails: MutableLiveData<RepositoryDetails> = MutableLiveData()
    val repositoryDetails: LiveData<RepositoryDetails> = _repositoryDetails

    private val githubService: GithubReposApi = run {
        val retrofit = Retrofit.Builder()
            .baseUrl(GithubReposApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        retrofit.create(GithubReposApi::class.java)
    }
    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = _errorMessage

    fun getRepoInfo(orgName: String, repoName: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                githubService.getRepositoryDetails(orgName, repoName).enqueue(
                    object : Callback<RepositoryDetails> {
                        override fun onResponse(
                            call: Call<RepositoryDetails>,
                            response: Response<RepositoryDetails>
                        ) {
                            if (!response.isSuccessful) {
                                _errorMessage.postValue(response.errorBody().toString())
                            } else {
                                _repositoryDetails.postValue(response.body())
                            }
                        }

                        override fun onFailure(call: Call<RepositoryDetails>, error: Throwable) {
                            _errorMessage.postValue(error.localizedMessage)
                        }

                    }
                )
            }
        }
    }
}