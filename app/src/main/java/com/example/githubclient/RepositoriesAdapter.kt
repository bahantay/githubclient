package com.example.githubclient

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.databinding.RepositoryListItemBinding
import com.example.githubclient.model.Repository

class RepositoriesAdapter(
    private var repositories: List<Repository>,
    private val onItemClickCallback: (orgName: String, repoName: String) -> Unit
) : RecyclerView.Adapter<RepositoriesAdapter.RepositoriesViewHolder>() {

    class RepositoriesViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        private val binding: RepositoryListItemBinding
        val root get() = binding.root
        val repositoryName get() = binding.repoName
        val repositoryDescription get() = binding.repoDescription

        init {
            binding = RepositoryListItemBinding.bind(view)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoriesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.repository_list_item, parent, false)
        return RepositoriesViewHolder(view)
    }

    override fun getItemCount(): Int = repositories.size

    override fun onBindViewHolder(holder: RepositoriesViewHolder, position: Int) {
        val repository = repositories[position]
        holder.repositoryName.text = repository.repositoryName
        holder.repositoryDescription.text = getDescriptionForViewHolder(repository.description)
        holder.root.setOnClickListener {
            onItemClickCallback(repository.organization, repository.repositoryName)
        }
    }

    fun setData(newRepositories: List<Repository>) {
        repositories = newRepositories
        notifyDataSetChanged()
    }

    private fun getDescriptionForViewHolder(originalDescription: String?): String =
        if (originalDescription.isNullOrBlank()) {
            "No description message"
        } else {
            originalDescription.splitToSequence(linesDelimiter)
                .take(DESCRIPTION_MAX_LINES)
                .joinToString(SPACE)
        }

    private companion object {
        const val DESCRIPTION_MAX_LINES = 3
        const val SPACE = " "
        val linesDelimiter: String = System.lineSeparator()
    }
}