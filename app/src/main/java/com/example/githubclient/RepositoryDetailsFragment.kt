package com.example.githubclient

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.githubclient.databinding.FragmentRepositoryDetailsBinding

class RepositoryDetailsFragment : Fragment() {
    private lateinit var viewModel: RepositoryDetailsViewModel
    private val args: RepositoryDetailsFragmentArgs by navArgs()
    private var _binding: FragmentRepositoryDetailsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this)[RepositoryDetailsViewModel::class.java]
        _binding = FragmentRepositoryDetailsBinding.inflate(inflater, container, false)
        subscribeToRepoDetailsReceive()
        return binding.root
    }

    private fun subscribeToRepoDetailsReceive() {
        viewModel.repositoryDetails.observe(viewLifecycleOwner) {
            binding.description.text = getString(R.string.description_template, it.description)
            binding.numberOfForks.text =
                getString(R.string.forks_template, it.numberOfForks)
            binding.numberOfIssues.text = getString(R.string.issues_template, it.openIssues)
            binding.numberOfWatchers.text =
                getString(R.string.watchers_template, it.numberOfWatchers)

            if (it.isFork) {
                binding.parentRepoName.apply {
                    visibility = View.VISIBLE
                    text = getString(R.string.parent_repository_template, it.parent!!.fullName)
                }
            }
            binding.repoName.text = it.name
        }

        viewModel.errorMessage.observe(viewLifecycleOwner) {
            it.toToast(requireContext())
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.getRepoInfo(args.orgName, args.repoName)
    }

}